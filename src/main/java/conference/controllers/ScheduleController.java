package conference.controllers;

import conference.entity.model.Presentation;
import conference.entity.model.Role;
import conference.entity.model.Schedule;
import conference.entity.model.User;
import conference.entity.repository.PresentationRepository;
import conference.entity.repository.RoomsRepository;
import conference.entity.repository.ScheduleRepository;
import conference.entity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

@Controller
public class ScheduleController {
    private static final long MIN_TO_MS = 2400000;

    private final ScheduleRepository scheduleRepository;
    private final RoomsRepository roomsRepository;
    private final PresentationRepository presentationRepository;
    private final UserRepository userRepository;

    @Autowired
    public ScheduleController(ScheduleRepository scheduleRepository, RoomsRepository roomsRepository, PresentationRepository presentationRepository, UserRepository userRepository) {
        this.scheduleRepository = scheduleRepository;
        this.roomsRepository = roomsRepository;
        this.presentationRepository = presentationRepository;
        this.userRepository = userRepository;
    }

    @PreAuthorize("hasAnyRole('ROLE_PRESENTER', 'ROLE_ADMINISTRATOR')")
    @GetMapping("/schedule/addConference")
    public String createSchedulePage(Model model) {
        model.addAttribute("schedule", new Schedule());
        model.addAttribute("rooms", roomsRepository.findAll());
        model.addAttribute("presentations", presentationRepository.findAll());
        return "createSchedule";
    }

    @PreAuthorize("hasAnyRole('ROLE_PRESENTER', 'ROLE_ADMINISTRATOR')")
    @PostMapping("/schedule/addConference")
    public String addNewPointOfSchedule(
            @ModelAttribute("schedule") Schedule schedule, RedirectAttributes redirectAttributes) {
        Optional<List<Schedule>> filteredSchedules =
                scheduleRepository.findSchedulesByRoomAndDate(schedule.getRoom(), schedule.getDate());
        if (filteredSchedules.isPresent()) {
            long timeOfNewSchedule = schedule.getDate().getTime();
            int count = 0;
            List<Schedule> schedules = filteredSchedules.get();
            schedules.sort(Comparator.comparing(Schedule::getDate));
            for (Schedule sched :
                    schedules) {
                if (timeOfNewSchedule >= sched.getDate().getTime() &&
                        timeOfNewSchedule < sched.getDate().getTime() + MIN_TO_MS
                        || timeOfNewSchedule + MIN_TO_MS > sched.getDate().getTime()
                        && timeOfNewSchedule + MIN_TO_MS <= sched.getDate().getTime() + MIN_TO_MS) {
                    count++;
                    break;
                }
            }
            if (count == 0) {
                scheduleRepository.save(schedule);
            } else {
                redirectAttributes.addFlashAttribute("message", "Please, change other time or date");
                return "redirect:/schedule/addConference";
            }
        } else {
            scheduleRepository.save(schedule);
        }

        return "redirect:/";
    }

    @PreAuthorize("hasAnyRole('ROLE_LISTENER', 'ROLE_PRESENTER', 'ROLE_ADMINISTRATOR')")
    @GetMapping("/schedule/{id}/signIn")
    public String signIn(@PathVariable("id") Long id) {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        if (schedule.isPresent()) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof UserDetails) {
                String username = ((UserDetails) principal).getUsername();
                Optional<User> userOptional = userRepository.findByUsername(username);
                if (userOptional.isPresent()) {
                    User user = userOptional.get();
                    boolean checkListener = false;
                    for (User listener :
                            schedule.get().getListeners()) {
                        if (listener.getUsername().equals(user.getUsername())) {
                            checkListener = true;
                        }
                    }
                    if (!checkListener) {
                        schedule.ifPresent(schedule1 -> {
                            schedule1.getListeners().add(user);
                            scheduleRepository.save(schedule1);
                        });
                    }
                }
            } else {
                return "login";
            }

            return "redirect:/schedule/{id}/details";
        } else {
           return "redirect:/";
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_PRESENTER', 'ROLE_LISTENER', 'ROLE_ADMINISTRATOR')")
    @GetMapping("/schedule/{id}/signOut")
    public String signOut(@PathVariable("id") Long id) {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            Optional<User> user = userRepository.findByUsername(username);
            schedule.ifPresent(schedule1 -> {
                schedule1.getListeners().remove(user.get());
                scheduleRepository.save(schedule1);
            });

        } else {
            return "login";
        }

        return "redirect:/schedule/{id}/details";

    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_LISTENER', 'ROLE_PRESENTER')")
    @GetMapping("/schedule/{id}/details")
    public String details(@PathVariable("id") Long id, Model model) {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            if (schedule.isPresent()) {
                boolean isListener = false;
                String username = ((UserDetails) principal).getUsername();
                for (User listener :
                        schedule.get().getListeners()) {
                    if (listener.getUsername().equals(username)) {
                        isListener = true;
                        break;
                    }
                }
                model.addAttribute("isListener", isListener);
                model.addAttribute("schedule", schedule.get());
            } else {
                return "404";
            }
            return "showSchedule";
        } else {
            return "login";
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    @GetMapping("/schedule/list")
    public String index(Model model) {
        Iterable<Schedule> schedules = scheduleRepository.findAll();
        model.addAttribute("scheduleList", schedules);
        return "scheduleList";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_PRESENTER')")
    @GetMapping("/schedule/{id}/delete")
    public String deleteSchedulePoint(@PathVariable("id") Long id) {
        Optional<Schedule> schedule = scheduleRepository.findById(id);
        if (schedule.isPresent()) {
            schedule.ifPresent(schedule1 -> schedule1.setListeners(null));
            scheduleRepository.save(schedule.get());
            scheduleRepository.deleteById(id);
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof UserDetails) {
                String username = ((UserDetails) principal).getUsername();
                boolean isAdmin = false;
                for (Role role :
                        userRepository.findByUsername(username).get().getRoles()) {
                    if (role.getCode().equals("ROLE_ADMINISTRATOR")) {
                        isAdmin = true;
                    }
                }
                if (isAdmin) {
                    return "redirect:/schedule/list";
                } else {
                    return "redirect:/schedule/mySchedule";
                }
            }
        } else {
            return "404";
        }
        return "index";
    }

    @PreAuthorize("hasAnyRole('ROLE_PRESENTER', 'ROLE_ADMINISTRATOR')")
    @GetMapping("/schedule/mySchedule")
    public String showMySchedule(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            if (userRepository.findByUsername(username).isPresent()) {
                List<Presentation> presentations =
                        presentationRepository.findByPresenter(userRepository.findByUsername(username).get());
                List<Schedule> schedules = new ArrayList<>();
                for (Presentation present :
                        presentations) {
                    schedules.addAll(scheduleRepository.findByPresentationId(present.getId()));
                }
                model.addAttribute("mySchedule", schedules);
            } else {
                return "404";
            }
        } else {
            return "login";
        }
        return "showMySchedule";
    }
}
