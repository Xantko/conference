package conference.controllers;

import conference.entity.model.Rooms;
import conference.entity.model.Schedule;
import conference.entity.repository.RoomsRepository;
import conference.entity.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.*;

@Controller
public class IndexController {

    private final RoomsRepository roomsRepository;
    private final ScheduleRepository scheduleRepository;

    @Autowired
    public IndexController(RoomsRepository roomsRepository, ScheduleRepository scheduleRepository) {
        this.roomsRepository = roomsRepository;
        this.scheduleRepository = scheduleRepository;
    }

    @GetMapping("/")
    public String index(Model model) {
        Iterable<Rooms> rooms = roomsRepository.findAll();
        List<List<Schedule>> scheduleList = new ArrayList<>();
        for (Rooms room:
             rooms) {
            scheduleList.add(scheduleRepository.findByRoomId(room.getId()));
        }
        scheduleList.forEach(schedules -> schedules.sort(Comparator.comparing(Schedule::getDate)));
        model.addAttribute("scheduleList", scheduleList);
        model.addAttribute("rooms", rooms);
        return "index";
    }

    @GetMapping("/403")
    public String accessDenied() {
        return "403";
    }


}
