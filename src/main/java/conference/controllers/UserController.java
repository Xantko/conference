package conference.controllers;

import conference.entity.model.Presentation;
import conference.entity.model.Role;
import conference.entity.model.Schedule;
import conference.entity.model.User;
import conference.entity.repository.PresentationRepository;
import conference.entity.repository.RolesRepository;
import conference.entity.repository.ScheduleRepository;
import conference.entity.repository.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Data
@Controller
public class UserController {

    private final UserRepository userRepository;
    private final RolesRepository rolesRepository;
    private final PresentationRepository presentationRepository;
    private final ScheduleRepository scheduleRepository;

    private final String ROLE_NAME = "ROLE_LISTENER";

    @Autowired
    public UserController(UserRepository userRepository, RolesRepository rolesRepository, PresentationRepository presentationRepository, ScheduleRepository scheduleRepository) {
        this.userRepository = userRepository;
        this.rolesRepository = rolesRepository;
        this.presentationRepository = presentationRepository;
        this.scheduleRepository = scheduleRepository;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    @GetMapping("/users")
    public String getAllUsers(Model model) {
        model.addAttribute("usersList", userRepository.findAll());
        return "usersList";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    @GetMapping("/users/{id}/profile")
    public String getById(@PathVariable("id") Long id, Model model) {
        if (userRepository.findById(id).isPresent()) {
            model.addAttribute("user", userRepository.findById(id).get());
        } else {
            return "404";
        }
        return "showUser";
    }

    @GetMapping("/users/addUser")
    public String createUserPage(Model model) {
        model.addAttribute("user", new User());
        return "createUser";
    }

    @PostMapping("/users/addUser")
    public String addUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "createUser";
        }
        user.setCredentialsNonExpired(true);
        user.setAccountNonLocked(true);
        user.setAccountNonExpired(true);
        user.setEnabled(true);
        user.setUsername(user.getUsername().toLowerCase());
        user.setPassword("{bcrypt}" + new BCryptPasswordEncoder().encode(user.getPassword()));
        Optional<Role> role = rolesRepository.findByCode(ROLE_NAME);
        role.ifPresent(role1 -> user.getRoles().add(role1));
        userRepository.save(user);
        return "redirect:/users";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    @GetMapping("/users/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            user.get().setRoles(null);
            List<Presentation> presentationList = presentationRepository.findAllPresentationsByPresenterId(id);
            for (Presentation present :
                    presentationList) {
                List<User> presenters = present.getPresenter();
                for (int i = 0; i < presenters.size(); i++) {
                    if (presenters.get(i).getId().equals(id)) {
                        presenters.remove(i);
                    }
                }
            }
            List<Schedule> schedules = scheduleRepository.findAllSchedulesByListenersId(id);
            for (Schedule schedule :
                    schedules) {
                List<User> listeners = schedule.getListeners();
                for (int i = 0; i < listeners.size(); i++) {
                    if (listeners.get(i).getId().equals(id)) {
                        listeners.remove(i);
                    }
                }
            }
            userRepository.save(user.get());
            userRepository.deleteById(id);
        } else {
            return "404";
        }
        return "redirect:/users";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    @GetMapping("/users/update/{id}")
    public String updateUser(@PathVariable("id") Long id, Model model) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            model.addAttribute("user", optionalUser.get());
            Iterable<Role> roles = rolesRepository.findAll();
            model.addAttribute("roles", roles);
        } else {
            return "404";
        }
        return "editUser";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    @PostMapping("/users/update/{id}")
    public String updateUser(@PathVariable("id") Long id, @ModelAttribute("user") User user) {
        if (userRepository.findById(id).isPresent()) {
            Long currentId = userRepository.findById(id).get().getId();
            user.setId(currentId);
            String currentPassword = userRepository.findById(id).get().getPassword();
            user.setPassword(currentPassword);
            userRepository.save(user);
        } else {
            return "404";
        }
        return "redirect:/users/" + user.getId() + "/profile";
    }
}
