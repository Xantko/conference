package conference.controllers;

import conference.entity.model.Presentation;
import conference.entity.model.Role;
import conference.entity.model.Schedule;
import conference.entity.model.User;
import conference.entity.repository.PresentationRepository;
import conference.entity.repository.RolesRepository;
import conference.entity.repository.ScheduleRepository;
import conference.entity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Optional;

@Controller
public class PresentationController {

    private PresentationRepository presentationRepository;
    private ScheduleRepository scheduleRepository;
    private UserRepository userRepository;
    private RolesRepository rolesRepository;

    private final String ROLE_NAME = "ROLE_PRESENTER";

    @Autowired
    public PresentationController(PresentationRepository presentationRepository,
                                  ScheduleRepository scheduleRepository,
                                  RolesRepository rolesRepository,
                                  UserRepository userRepository) {
        this.presentationRepository = presentationRepository;
        this.scheduleRepository = scheduleRepository;
        this.userRepository = userRepository;
        this.rolesRepository = rolesRepository;
    }

    @PreAuthorize("hasAnyRole('ROLE_PRESENTER', 'ROLE_ADMINISTRATOR')")
    @GetMapping("/presentations/addPresentation")
    public String createPresentation(Model model) {
        model.addAttribute("presentation", new Presentation());
        Optional<Role> role = rolesRepository.findByCode(ROLE_NAME);
        if (role.isPresent()) {
            List<User> presenters = userRepository.findByRoles(role.get());
            model.addAttribute("presenters", presenters);
        }
        return "createPresentation";
    }

    @PreAuthorize("hasAnyRole('ROLE_PRESENTER', 'ROLE_ADMINISTRATOR')")
    @PostMapping(value = "/presentations/addPresentation")
    public RedirectView addPresentation(@ModelAttribute("presentation") Presentation presentation,
                                        @RequestParam(value = "presenters", required = false) long[] presenters) {
        if (presenters != null) {
            for (long presenter : presenters) {
                Optional<User> pres = userRepository.findById(presenter);
                pres.ifPresent(user -> presentation.getPresenter().add(user));
            }
        }
        presentationRepository.save(presentation);
        return new RedirectView("/presentations/myPresentations");
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR')")
    @GetMapping("/presentations/presentationsList")
    public String presentationsList(Model model) {
        Iterable<Presentation> presentations = presentationRepository.findAll();
        model.addAttribute("presentationsList", presentations);
        return "presentationsList";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_PRESENTER')")
    @GetMapping("/presentations/presentation/{id}")
    public String getPresentationById(@PathVariable("id") Long id, Model model) {
        Optional<Presentation> presentation = presentationRepository.findById(id);
        if (presentation.isPresent()) {
            model.addAttribute("presentation", presentation.get());
        } else {
            return "404";
        }
        return "showPresentation";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_PRESENTER')")
    @GetMapping("/presentations/delete/{id}")
    public String deletePresentation(@PathVariable("id") Long id) {
        Optional<Presentation> presentation = presentationRepository.findById(id);
        if (presentation.isPresent()) {
            List<Schedule> schedules = scheduleRepository.findByPresentationId(id);
            schedules.forEach(schedule -> {
                schedule.setListeners(null);
                scheduleRepository.save(schedule);
                scheduleRepository.delete(schedule);
            });
            presentation.ifPresent(presentation1 -> presentation1.setPresenter(null));
            presentationRepository.save(presentation.get());
            presentationRepository.deleteById(id);
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof UserDetails) {
                String username = ((UserDetails) principal).getUsername();
                boolean isAdmin = false;
                for (Role role :
                        userRepository.findByUsername(username).get().getRoles()) {
                    if (role.getCode().equals("ROLE_ADMINISTRATOR")) {
                        isAdmin = true;
                    }
                }
                if (isAdmin) {
                    return "redirect:/presentations/presentationsList";
                } else {
                    return "redirect:/presentations/myPresentations";
                }
            }
        } else {
            return "404";
        }
        return "index";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_PRESENTER')")
    @GetMapping("/presentations/update/{id}")
    public String updatePresentation(@PathVariable("id") Long id, Model model) {
        Optional<Presentation> presentation = presentationRepository.findById(id);
        if (presentation.isPresent()) {
            model.addAttribute("presentation", presentation.get());
            Optional<Role> role = rolesRepository.findByCode(ROLE_NAME);
            role.ifPresent(role1 -> model.addAttribute("presenters", userRepository.findByRoles(role1)));
        } else {
            return "404";
        }
        return "editPresentation";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMINISTRATOR', 'ROLE_PRESENTER')")
    @PostMapping("/presentations/update/{id}")
    public String updatePresentation(@PathVariable("id") Long id, @ModelAttribute("presentation") Presentation presentation) {
        if (presentationRepository.findById(id).isPresent()) {
            presentation.setId(presentationRepository.findById(id).get().getId());
            presentationRepository.save(presentation);
        }
        return "redirect:/presentations/presentation/" + presentation.getId();
    }

    @PreAuthorize("hasAnyRole('ROLE_PRESENTER', 'ROLE_ADMINISTRATOR')")
    @GetMapping("/presentations/myPresentations")
    public String showMyPresentations(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            if (userRepository.findByUsername(username).isPresent()) {
                List<Presentation> presentations =
                        presentationRepository.findByPresenter(userRepository.findByUsername(username).get());
                model.addAttribute("myPresentations", presentations);
            } else {
                return "404";
            }
        } else {
            return "login";
        }
        return "showMyPresentations";
    }
}
