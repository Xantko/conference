package conference.entity.repository;

import conference.entity.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolesRepository extends CrudRepository<Role, Long> {
    Optional<Role> findByCode(String code);
}
