package conference.entity.repository;

import conference.entity.model.Role;
import conference.entity.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{

    List<User> findByLastName(@Param("name") String lastName);

    List<User> findByFirstName(String firstName);

    Optional<User> findByUsername(String username);

    List<User> findByRoles(Role role);

    Optional<User> findById(Long id);

    Optional<User> findByEmail(String email);
}
