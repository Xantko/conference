package conference.entity.repository;

import conference.entity.model.Presentation;
import conference.entity.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public interface PresentationRepository  extends CrudRepository<Presentation, Long> {

    List<Presentation> findAllPresentationsByPresenterId(Long id);

    List<Presentation> findByPresenter(User presenter);
}
