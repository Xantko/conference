package conference.entity.repository;

import conference.entity.model.Presentation;
import conference.entity.model.Rooms;
import conference.entity.model.Schedule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface ScheduleRepository extends CrudRepository<Schedule, Long> {

    Schedule findByDate(Date date);

    void deleteByPresentation(Presentation presentation);

    List<Schedule> findByPresentationId(Long id);

    void deleteById(Long id);

    List<Schedule> findAllSchedulesByListenersId(Long id);

    Optional<List<Schedule>> findSchedulesByRoomAndDate(@NotEmpty Rooms room, @NotEmpty Date date);

    List<Schedule> findByRoomId(Long id);

}