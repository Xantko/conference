package conference.entity.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Presentation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String namePresentation;

    @ManyToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<User> presenter = new ArrayList<>();

    public Presentation() {}

    public Presentation(String namePresentation) {
        this.namePresentation = namePresentation;
    }



    @Override
    public String toString() {
        return namePresentation ;
    }
}
