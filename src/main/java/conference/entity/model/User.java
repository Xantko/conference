package conference.entity.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity(name = "users")
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 30, min = 3, message = "Your username must be between 3 and 30 characters")
    @NotEmpty(message = "Please provide username")
    @UniqueUsername
    private String username;

    @NotNull
    @Size(min = 6, message = "Your password must include a minimum of six characters!")
    @NotEmpty(message = "Please provide a password")
    private String password;

    @NotNull
    @Size(min = 2, message = "Your first name must include a minimum of 2 characters")
    private String firstName;

    @NotNull
    @Size(min = 2, message = "Your last name must include a minimum of 2 characters")
    private String lastName;

    @UniqueEmail
    @Email(message = "Please provide a valid e-mail")
    @NotEmpty(message = "Please provide an e-mail")
    private String email;

    @NotNull
    private boolean accountNonExpired;
    @NotNull
    private boolean accountNonLocked;
    @NotNull
    private boolean credentialsNonExpired;
    @NotNull
    private boolean enabled;

    @Override
    public String toString() {
        return  firstName + " " + lastName;
    }

    public User(@NotNull String username, @NotNull String password, @NotNull String firstName, @NotNull String lastName, @NotNull boolean accountNonExpired, @NotNull boolean accountNonLocked, @NotNull boolean credentialsNonExpired, @NotNull boolean enabled, @Email(message = "Please provide a valid e-mail") @NotEmpty(message = "Please provide an e-mail") String email) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.enabled = enabled;
        this.email = email;
    }

    @ManyToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private Set<Role> roles = new HashSet<>();

    public User(String login, String pass, String firstName, String lastName, @Email(message = "Please provide a valid e-mail") @NotEmpty(message = "Please provide an e-mail") String email) {
        this.username = login;
        this.password = pass;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }
}


