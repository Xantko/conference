package conference.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String code;

    @Override
    public String getAuthority() {
        return code;
    }

    @Override
    public String toString() {
        return code + " ";
    }
}