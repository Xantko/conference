package conference.entity.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


@Data
@NoArgsConstructor
@Entity
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotEmpty
    private Long id;

    @NotEmpty
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @NotEmpty
    @ManyToOne
    private Rooms room;

    @ManyToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<User> listeners = new ArrayList<>();

    @NotEmpty
    @ManyToOne
    private Presentation presentation;

    public Schedule(Date date, Rooms room, Presentation presentation) {
        this.date = date;
        this.room = room;
        this.presentation = presentation;
    }

    public void setDate(String date) {
        date = date.replace('T', ' ');
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        try {
            this.date = format.parse(date);
        } catch (Exception e) {
            System.out.println("Parse date exception");
        }
    }

    public String getStringTime(Date time){
        DateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        return timeFormat.format(time);
    }

    public String getStringDate(Date date){
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
        return dateFormat.format(date);
    }
}
